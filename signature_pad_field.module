<?php
/**
 * @file
 * Main file for the Signature Pad module.
 * @author Jorge Castedo <jorgeh@newtarget.com>
 */

function signature_pad_field_help($path, $arg) {
  switch ($path) {
    case "admin/help#signature_pad_field":
      return '<p>' . t("Easy Signature Mobile") . '</p>';
      break;
  }
}

function signature_pad_field_element_info() {
  $type = array();

  $type['signature_pad_field'] = array(
    '#input' => TRUE,
    '#after_build' => array('signature_pad_field_after_build'),
    '#theme' => 'signature_pad_field',
  );

  return $type;
}

function signature_pad_field_webform_component_info() {
  $components = array();

  $components['signature_pad_field'] = array(
    'label'       => t('Signature Pad'),
    'description' => t('Signature Pad field.'),
    'features'    => array(
      'csv' => FALSE,
      'email' => TRUE,
      'email_address' => FALSE,
      'email_name' => FALSE,
      'required' => TRUE,
      'conditional' => TRUE,
      'group' => FALSE,
      'attachment' => TRUE,
    ),
    'file' => 'include/signature_pad_field.component.inc',
  );

  return $components;

}

function signature_pad_field_field_info() {
  return array(
    'signature_pad_field' => array(
      'label' => t('Signature Pad field'),
      'default_widget' => 'signature_pad_field_default',
      'default_formatter' => 'signature_pad_field_formatter',
    ),
  );
}

function signature_pad_field_field_formatter_info() {
  return array(
    'signature_pad_field_formatter' => array(
      'label' => t('Signature Pad image'),
      'field types' => array('signature_pad_field'),
    ),
  );
}

function signature_pad_field_field_widget_info() {
  return array(
    'signature_pad_field_default' => array(
      'label' => t('Signature field widget'),
      'field types' => array('signature_pad_field'),
    ),
  );
}

function signature_pad_field_theme() {
  $items = array();

  $items['signature_pad_field'] = array(
    'template' => 'template/signature_pad_field',
    'render element' => 'element',
  );

  $items['signature_pad_field_display'] = array(
    'variables' => array(
      'data' => NULL,
      'settings' => array(),
    ),
  );

  drupal_alter('signature_pad_field_theme', $items);

  return $items;
}

function signature_pad_field_after_build($element, $form_state) {
  
  $viewport = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'viewport',
      'content' => 'width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no'
    )
  );

  $webCapable = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'apple-mobile-web-app-capable',
      'content' => 'yes'
    )
  );

  $webStatus = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'apple-mobile-web-app-status-bar-style',
      'content' => 'black'
    )
  );

  drupal_add_html_head($viewport, 'site_viewport');
  drupal_add_html_head($webCapable, 'site_capable');
  drupal_add_html_head($webStatus, 'site_status');

  $mPath = drupal_get_path('module', 'signature_pad_field');

  drupal_add_css($mPath . '/css/signature-pad.css');
  drupal_add_js($mPath . '/js/signature_pad_field.js');
  drupal_add_js($mPath . '/js/app.js');

  return $element;
}

function signature_pad_field_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  switch ($display['type']) {
    case 'signature_pad_field_formatter':
      foreach ($items as $delta => $item) {
        $element[$delta] = array(
          '#markup' => theme('signature_pad_field_display', array(
            'data' => $item['data'],
            'settings' => $instance['widget']['settings'],
          )),
        );
      }
    break;
  }

  return $element;
}

function signature_pad_field_field_widget_settings_form($field, $instance) {
  $widget = $instance['widget'];
  $settings = $widget['settings'];
  $form = array();
  return $form;
}

function signature_pad_field_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $value = isset($items[$delta]['signature_pad_field']) ? $items[$delta]['signature_pad_field'] : '';

  $widget = $element;
  $widget['#delta'] = $delta;

  $widget += array(
    '#type' => 'signature_pad_field',
    '#default_value' => isset($items[$delta]['data']) ? $items[$delta]['data'] : NULL,
  );

  $element['data'] = $widget;
  return $element;
}

function signature_pad_field_form_alter(&$form, &$form_state, $form_id) {
  drupal_alter('signature_pad_field_form_alter', $form, $form_state, $form_id);
}