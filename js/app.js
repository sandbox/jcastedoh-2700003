(function ($) {
    Drupal.behaviors.signature_padBehavior = {
        attach: function(context, settings){

            function signatureInstance(wrapperID){
                var wrapper = document.getElementById(wrapperID),
                clearButton = wrapper.querySelector("[data-action=clear]"),
                textData = $(wrapper).find('textarea'),
                canvas = wrapper.querySelector("canvas.main"), 
                canvasHidden = wrapper.querySelector("canvas.cropped"), signaturePad;

                function resizeCanvas(){
                    var ratio =  Math.max(window.devicePixelRatio || 1, 1);
                    canvas.width = canvas.offsetWidth * ratio;
                    canvas.height = canvas.offsetHeight * ratio;
                    canvas.getContext("2d").scale(ratio, ratio);
                }

                function is_touch_device() {
                    return 'ontouchstart' in window || navigator.maxTouchPoints;
                }

                function gotSignature(){
                    $(wrapper).find('canvas.cropped').attr({'width': $(wrapper).find('canvas.main').width(), 'height': $(wrapper).find('canvas.main').height()});
                    var i = 0;
                    var Interval = setInterval(function(){
                        if(i <= 10){ signaturePadHidden.fromDataURL(signaturePad.toDataURL()); i++; } 
                        else { clearInterval(Interval); textData.text(signaturePadHidden.removeBlanks()); }
                    }, 1);                    
                }
                
                window.onresize = resizeCanvas;
                resizeCanvas();

                signaturePad = new SignaturePad(canvas);
                signaturePadHidden = new SignaturePad(canvasHidden);

                clearButton.addEventListener("click", function (event) {
                    signaturePad.clear();
                    signaturePadHidden.clear();
                    textData.text('');
                });

                if(is_touch_device()){ canvas.addEventListener("touchend", function(event){ gotSignature(); }); } 
                else { canvas.addEventListener("mouseup", function(event){ gotSignature(); }); }
            }

            $('.webform-component-signature_pad').each(function(){
                var wrapperID = $(this).find('.m-signature-pad').attr('id');
                signatureInstance(wrapperID);
            });

        }
    };
})(jQuery);