<?php
/**
 * @file
 * Signature Pad element template file
 */
?>

<div id="<?php echo 'signature-pad-field-' . $element['#id']; ?>" class="m-signature-pad">
	<div class="m-signature-pad-field--body">
		<canvas class="main"></canvas>
	</div>
	<div class="m-signature-pad-field--footer">
		<canvas class="hidden cropped"></canvas>
		<textarea name='<?php echo $element['#name'] ?>' id='<?php echo $element['#id'] ?>' class='output hidden'></textarea>
		<div class="description">Draw your signature</div>
		<button type="button" class="button clear" data-action="clear">Clear</button>
	</div>
</div>
<?php if (!empty($element['#description']))  echo "<div class='description'>" . $element['#description'] . "</div>"; ?>

